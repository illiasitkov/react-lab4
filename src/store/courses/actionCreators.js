import * as actionTypes from './actionTypes';

export const coursesFetched = (courses) => ({
	type: actionTypes.COURSES_FETCHED,
	payload: {
		courses,
	},
});

export const courseAdded = (newCourse) => ({
	type: actionTypes.COURSE_ADDED,
	payload: {
		newCourse,
	},
});

export const courseDeleted = (courseId) => ({
	type: actionTypes.COURSE_DELETED,
	payload: {
		courseId,
	},
});

export const courseUpdated = (course) => ({
	type: actionTypes.COURSE_UPDATED,
	payload: {
		course,
	},
});
