import * as actionTypes from './actionTypes';

const coursesInitialState = [];

export const coursesReducer = (courses = coursesInitialState, action) => {
	switch (action.type) {
		case actionTypes.COURSES_FETCHED:
			return action.payload.courses;
		case actionTypes.COURSE_ADDED:
			return [...courses, action.payload.newCourse];
		case actionTypes.COURSE_DELETED:
			return courses.filter((course) => course.id !== action.payload.courseId);
		case actionTypes.COURSE_UPDATED:
			const withoutCourse = courses.filter(
				(course) => course.id !== action.payload.course.id
			);
			return withoutCourse.concat(action.payload.course);
		default:
			return courses;
	}
};
