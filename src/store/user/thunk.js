import { logoutUser } from '../../services/AuthService';
import { fetchedUser, userLoggedOut } from './actionCreators';
import { TOKEN_LOCAL_STORAGE } from '../../constants';
import { loadUser } from '../../services/UserService';
import { ADMIN_EMAIL } from '../constants';
import { UserRoles } from './userRoles';

export const userLoggedOutThunk = (bearerToken) => async (dispatch) => {
	try {
		const res = await logoutUser(bearerToken);
		if (res.status === 200) {
			dispatch(userLoggedOut());
		} else {
			alert(res.status + ' ' + res.statusText);
		}
	} catch (e) {
		console.log(e);
	}
};

export const getCurrentUserThunk = () => async (dispatch) => {
	try {
		const token = localStorage.getItem(TOKEN_LOCAL_STORAGE);
		const res = await loadUser(token);
		const obj = await res.json();
		if (res.status === 200) {
			const { name, email } = obj.result;
			const role = email === ADMIN_EMAIL ? UserRoles.ADMIN : UserRoles.REGULAR;
			dispatch(fetchedUser(name, email, token, role));
		} else {
			alert('Loading current user failed');
		}
	} catch (err) {
		alert(err);
		console.log(err);
	}
};
