const url = 'http://localhost:4000';

export const fetchCourses = () => {
	return fetch(`${url}/courses/all`);
};

export const deleteCourse = (courseId, bearerToken) => {
	return fetch(`${url}/courses/${courseId}`, {
		method: 'DELETE',
		headers: {
			Authorization: bearerToken,
		},
	});
};

export const updateCourse = (course, bearerToken) => {
	return fetch(`${url}/courses/${course.id}`, {
		method: 'PUT',
		body: JSON.stringify(course),
		headers: {
			'Content-Type': 'application/json',
			Authorization: bearerToken,
		},
	});
};

export const createCourse = (course, bearerToken) => {
	return fetch(`${url}/courses/add`, {
		method: 'POST',
		body: JSON.stringify(course),
		headers: {
			'Content-Type': 'application/json',
			Authorization: bearerToken,
		},
	});
};

export const fetchAuthors = () => {
	return fetch(`${url}/authors/all`);
};

export const createAuthor = (name, bearerToken) => {
	return fetch(`${url}/authors/add`, {
		method: 'POST',
		headers: {
			Authorization: bearerToken,
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({ name }),
	});
};
