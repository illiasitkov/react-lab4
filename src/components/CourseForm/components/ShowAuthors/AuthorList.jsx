import React from 'react';
import PropTypes from 'prop-types';

const AuthorList = ({ authorViewList, title }) => {
	return (
		<div>
			<h4 className='text-center mb-4'>{title}</h4>
			{authorViewList}
			{authorViewList.length <= 0 && (
				<p className='text-center'>Author list is empty</p>
			)}
		</div>
	);
};

AuthorList.propTypes = {
	authorViewList: PropTypes.array.isRequired,
	title: PropTypes.string.isRequired,
};

export default AuthorList;
