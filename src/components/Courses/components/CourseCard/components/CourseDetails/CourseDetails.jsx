import { durationPipeToString } from '../../../../../../helpers/durationPipe';
import { dateConverterPipe } from '../../../../../../helpers/datePipe';
import CourseDetail from './components/CourseDetail/CourseDetail';
import { AUTHORS, CREATED, DURATION } from '../../../../../../constants';
import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { getAuthors } from '../../../../../../store/selectors';

const CourseDetails = ({ course, showId }) => {
	const authors = useSelector(getAuthors);

	const authorsList = course.authors.map(
		(id) => authors.filter((a) => a.id === id)[0].name
	);
	const authorsListString = authorsList.join(', ');
	const duration = durationPipeToString(course.duration);
	const createdDate = dateConverterPipe(course.creationDate);

	return (
		<section>
			{showId && <CourseDetail title='ID' text={course.id} />}
			<CourseDetail title={AUTHORS} text={authorsListString} />
			<CourseDetail title={DURATION} text={duration + ' hours'} />
			<CourseDetail title={CREATED} text={createdDate} />
		</section>
	);
};

CourseDetails.propTypes = {
	showId: PropTypes.bool.isRequired,
	course: PropTypes.shape({
		authors: PropTypes.arrayOf(PropTypes.string).isRequired,
		id: PropTypes.string.isRequired,
		duration: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
			.isRequired,
		creationDate: PropTypes.string.isRequired,
	}),
};

CourseDetails.defaultProps = {
	showId: false,
};

export default CourseDetails;
