export const changeHandler = (setter) => (e) => {
	setter(e.target.value);
};
